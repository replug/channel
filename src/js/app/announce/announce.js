app.controller('AnnounceCtrl', ['$scope', function($scope) {

  $scope.labels = [
    {name: 'Operations', filter:'operations', color:'#23b7e5'},
    {name: 'Human Resource', filter:'hrt', color:'#7266ba'},
    {name: 'Devotion', filter:'devotion', color:'#fad733'},
    {name: 'Finance', filter:'finance', color:'#27c24c'}
  ];

  $scope.addLabel = function(){
    $scope.labels.push(
      {
        name: $scope.newLabel.name,
        filter: angular.lowercase($scope.newLabel.name),
        color: '#ccc'
      }
    );
    $scope.newLabel.name = '';
  }

  $scope.labelClass = function(label) {
    return {
      'b-l-info': angular.lowercase(label) === 'operations',
      'b-l-primary': angular.lowercase(label) === 'hrt',
      'b-l-warning': angular.lowercase(label) === 'devotion',
      'b-l-success': angular.lowercase(label) === 'finance'
    };
  };

}]);

app.controller('AnnounceListCtrl', ['$scope', 'announce', '$stateParams', function($scope, announce, $stateParams) {
  $scope.fold = $stateParams.fold;
  announce.all().then(function(announcements){
    $scope.anncs = announcements;
  });
}]);

app.controller('AnnounceDetailCtrl', ['$scope', 'announce', '$stateParams', function($scope, announce, $stateParams) {
  announce.get($stateParams.mailId).then(function(mail){
    $scope.mail = mail;
  })
}]);

app.controller('MailNewCtrl', ['$scope', function($scope) {
  $scope.mail = {
    to: '',
    subject: '',
    content: ''
  }
  $scope.tolist = [
    {name: 'James', email:'james@gmail.com'},
    {name: 'Luoris Kiso', email:'luoris.kiso@hotmail.com'},
    {name: 'Lucy Yokes', email:'lucy.yokes@gmail.com'}
  ];
}]);

angular.module('app').directive('labelColor', function(){
  return function(scope, $el, attrs){
    $el.css({'color': attrs.color});
  }
});
