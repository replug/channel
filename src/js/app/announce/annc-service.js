// A RESTful factory for retreiving mails from 'announcements.json'
app.factory('announce', ['$http', function ($http) {
  var path = 'js/app/announce/announcements.json';
  var announcements = $http.get(path).then(function (resp) {
    return resp.data.announcements;
  });

  var factory = {};
  factory.all = function () {
    return announcements;
  };
  factory.get = function (id) {
    return announcements.then(function(announcements){
      for (var i = 0; i < announcements.length; i++) {
        if (announcements[i].id == id) return announcements[i];
      }
      return null;
    })
  };
  return factory;
}]);
